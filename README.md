# Requirements

* Rust (Edition 2021)
* Cargo

The whole toolchain is available via [rustup](https://rustup.rs/)

# Running

Since this is a library there's nothing to run other than the tests via:

```shell
cargo test
```
