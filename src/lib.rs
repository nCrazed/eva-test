/// Validate a given string as an NHS number, will return false on unexpected characters, incorrect
/// length, or if it doesn't pass the module 11 algorithm
///
/// Does NOT handle missing leading 0s
///
/// TODO: return type could be changed to conversion into a NhsNumber type that can then be safely
///       used throughout the system
pub fn validate(nhs_number: &str) -> bool {
    if nhs_number.len() != 10 {
        // If the number is not exactly 10 characters then it can't be valid
        return false;
    }

    const FACTORS: [u32; 9] = [10, 9, 8, 7, 6, 5, 4, 3, 2];

    // Extract valid digits from the given string
    let mut digits: Vec<u32> = nhs_number
        .chars()
        .map(|c| c.to_string().parse())
        .filter_map(|parsed| parsed.ok()) // dropping non-digit characters here
        .collect();

    if digits.len() != 10 {
        // Second length check after we've extract only digits
        // The previous check could potentially be removed by replacing the above code with a for
        // loop and keeping track of/short-circuiting on errors
        return false;
    }

    let validator = digits
        .pop()
        .expect("Popping from a vector with 10 items should never fail");


    // Steps 1 & 2
    let weighted_sum: u32 = digits
        .iter()
        .zip(FACTORS.iter())
        .map(|(digit, factor)| digit * factor)
        .sum();

    // Step 3
    let remainder = weighted_sum % 11;

    // Steps 4 & 5
    match 11 - remainder {
        11 => {
            dbg!("Special case where remainder is 0");
            // TODO: need a test case for this
            validator == 0
        }
        10 => false,
        check_digit => validator == check_digit,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: Switch to parametrised tests for better debug output
    #[test]
    fn test_validation() {
        // Valid
        assert_eq!(validate("5990128088"), true);
        assert_eq!(validate("1275988113"), true);
        assert_eq!(validate("4536026665"), true);

        // Corrupted - not passing the module 11 algorithm
        assert_eq!(validate("5990128087"), false);
        assert_eq!(validate("4536016660"), false);

        // Invalid
        assert_eq!(validate("125990128087"), false);
        assert_eq!(validate("1288113"), false);
        assert_eq!(validate("abcdefghij"), false);
        assert_eq!(validate("a5990128088"), false);
    }
}
